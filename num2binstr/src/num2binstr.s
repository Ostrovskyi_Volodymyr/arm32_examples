.section .text
.global _start

_start:
	ldr r1, =buf
	ldr r2, =num
	ldrh r2, [r2]
	mov r3, #16
	_lp:
		mov r4, r2
		and r4, r4, #0x8000
		mov r4, r4, lsr #15
		add r4, r4, #'0'
		strb r4, [r1], #1
		mov r2, r2, lsl #1
		sub r3, r3, #1
		cmp r3, #0
		bhi _lp
	mov r4, #'\n'
	strb r4, [r1]
	mov r0, #1
	ldr r1, =buf
	mov r2, #17
	mov r7, #4
	svc 0

	mov r7, #1
	svc 0

.section .data
	num: .hword 12876

.section .bss	
	buf: .fill 17, 1, 0
