.section .text
.global _start

.macro puts string, length
	@ define constants
	.equ stdout, 0
	.equ sys_write, 0x4 @ linux system call number (write)

	mov r0, #stdout
	@ slash - get value of macro paramater
	ldr r1, =\string
	mov r2, #\length
	mov r7, #sys_write
	svc 0
.endm

_start:
	puts string, length
	mov r7, #1
	svc 0

.section .data
	string: .ascii "Hello, Linux!\n"
	@ dot means current address
	.equ length, . - string @ current address minus address of string label
