.section .text
.global _start

_start:
	mov r10, #3 @ rows
	mov r9, #3 @ columns
	ldr r8, =matrix
	mov r7, #0 @ i
	_for_i:
		mov r6, r7 @ j
		ldr r5, [r8, r7, LSL #2] @ r5 - matrix[i]
		_for_j:
			ldr r4, [r8, r6, LSL #2] @ r4 - matrix[j]

			ldr r3, [r5, r6, LSL #2] @ r3 - matrix[i][j]
			ldr r2, [r4, r7, LSL #2] @ r2 - matrix[j][i]
			str r2, [r5, r6, LSL #2] @ matrix[i][j] = r2
			str r3, [r4, r7, LSL #2] @ matrix[j][i] = r3

			add r6, r6, #1
			cmp r6, r9
			blt _for_j
		add r7, r7, #1
		cmp r7, r10
		blt _for_i
_exit:
	mov r7, #1
	svc 0

.section .data
	row1: .word 1, 2, 3
	row2: .word 4, 5, 6
	row3: .word 7, 8, 9
	matrix: .word row1, row2, row3
