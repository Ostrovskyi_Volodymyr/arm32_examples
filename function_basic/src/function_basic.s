.section .text
.global arm_abs

@ the absolute value of a number
arm_abs:
	push {lr} @ push return address onto the stack
	movs r1, r0, LSR #31 @ read sign bit
	negne r0, r0 @ if r1 == 1 then mov -r0 to r0
	pop {lr} @ pop return address
	bx lr @ return
