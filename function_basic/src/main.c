#include <stdio.h>

extern int arm_abs(int v);

int main(void) {
	int a = -5, b = 5;
	printf("|a| = %d, |b| = %d\n", arm_abs(a), arm_abs(b));
	return 0;
}
