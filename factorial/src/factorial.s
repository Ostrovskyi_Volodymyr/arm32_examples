.section .text
.global arm_factorial

arm_factorial:
	@ if num == 0 then return 1
	cmp r0, #0
	moveq r0, #1
	bxeq lr

	push {r1-r3, lr}
	mov r1, r0
	mov r2, #1
	mov r0, #1
	_cl:
		@ unsigned multiplication
		@ umull result_low, result_high, op1, op2
		@ result_low - first 32bit
		@ result_high - second 32bit
		umull r0, r3, r2, r0 @ r0 = r2 * r0
		add r2, r2, #1
		cmp r2, r1
		ble _cl
	pop {r1-r3, lr}
	bx lr
