#include <stdio.h>

extern int arm_factorial(int n);

int main(void) {
	printf("6! = %d\n", arm_factorial(6));
	printf("1! = %d\n", arm_factorial(1));
	printf("0! = %d\n", arm_factorial(0));
	return 0;
}
